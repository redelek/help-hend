---
title:  Prosta instrukcja jak poradzić sobie z pakowaniem i rozpakowywaniem w linux (terminal)
...

# 1. Pakowanie

`# tar`

```{r, engine='bash', count_lines}
[user@demo]$tar cvf nazwa_archiwum.tar /home/katalog/
```

`# tgz`

```{r, engine='bash', count_lines}
[user@demo]$tar cvzf nazwa_archiwum.tgz /home/katalog/ 
```

`# tar.bz2`

```{r, engine='bash', count_lines}
[user@demo]$tar cvfj nazwa_archiwum.bz2 /home/katalog/
```

# 1a. Rozpakowywanie

`# tar`

```{r, engine='bash', count_lines}
[user@demo]$tar -xvf nazwa_archiwum.tar
```

`# tgz`

```{r, engine='bash', count_lines}
[user@demo]$tar -xvf nazwa_archiwum.tgz /home/katalog/
```

`# tar.bz2`

```{r, engine='bash', count_lines}
[user@demo]$tar -xvf nazwa_archiwum.bz2 /home/katalog/
```

# 1b. Listowanie

`# tar,tgz,bz2`

```{r, engine='bash', count_lines}
[user@demo]$tar -tvf nazwa_archiwum.tar,tgz,bz2
```
