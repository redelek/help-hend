## KOMENDY do kart sieciowych

### SPRAWDZANIE
ip set link {interface} up - podnosi

ip set link {interface} down - kłądzie

ip a s - pokazuje wszystkie interfejsy

### DODAWANIE IP
ip a add {ip_addr/mask} dev {interface} - dodawanie IP

ip addr add brd {ADDDRESS-HERE} dev {interface} - dodawanie broadcast

### KASOWANIE IP
ip a del {ipv6_addr_OR_ipv4_addr} dev {interface} - kasowanie IP

ip -s -s a f to {ADDDRESS-HERE} - opuszczanie adresu IP

### MODYFIKACJA MTU
ip link set mtu {NUMBER} dev {DEVICE} - zmiana MTU

### MODYFIKACJA ARP
ip neigh add {IP-HERE} lladdr {MAC/LLADDRESS} dev {DEVICE} nud {STATE} - jak dodać ARP

ip neigh del {IPAddress} dev {DEVICE} - usuwanie ARP

ip -s -s n f {IPAddress} - upiszczanie ARP

### ROUTING
ip -r - pokazanie routing

ip r list - pokazywanie routing-u

ip route add {NETWORK/MASK} via {GATEWAYIP}

ip route add {NETWORK/MASK} dev {DEVICE}

ip route add default {NETWORK/MASK} dev {DEVICE}

ip route add default {NETWORK/MASK} via {GATEWAYIP}

ip route del default

ip route del {ADDDRESS-HERE} dev eth0

### MODYFIKACJA MACADDRESS
NIC="eno1" ## <-- My NIC name ##

ip link show $NIC

ip link set dev $NIC down

### set new MAC address ##
ip link set dev $NIC address XX:YY:ZZ:AA:BB:CC

ip link set dev $NIC up

------------------------
## KOMENDY iptables 
------------------------

### Listowanie i numerowanie regol
iptables -L -nv --line-numbers

### Blokowanie portu z konkretnego ip
iptables -I INPUT -p udp --destination-port 53 -s 192.168.2.196 -j DROP

### Kasowanie wpisu
iptables -D INPUT 3

-------------------------
## KOMENDY firewalld 
-------------------------



