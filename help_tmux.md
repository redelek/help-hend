### Tmux - czyli czary w konsoli ###

**Instalacja pakietu**

`[root@demo]# apt install tmux`

**NOWA SESJA**

`[root@demo]# tmux`

`[root@demo]# tmux new -s NAZWA_SESJI`


**DZIELENIE OKNA**

''ctrl+b % - podział okna w poziomie''

''ctrl+b " - podział okna w pionie (górny cudzysłów)''

''ctrl-b { (odwracanie kolejności okien)''

''ctrl-b } (przywracanie kolejności okien)''

''ctrl-b q - sprawdzanie numerów paneli''

''ctrl-b strzałka(prawo,lewo,góra,dół) poruszanie się pomiędzy panelami''

''ctrl-b [ - zmiana układu okien przenoszenie ''

''ctrl-b w - numerowanie okien''

''ctrl+b c — dodawanie nowego konta''

''ctrl+b n — przejście do kolejnego panelu''

''ctrl+b p — powrót do poprzedniego panelu''

''ctrl+b , - zmiana nazwy okna''

**Zamykanie sesji tmux**

''ctrl+b d - wyście i pozostawienie w seaji w tle''

''exit - w samej sesji (wyjście i zamknięcie okna sesji)''

`[root@demo]# tmux kill-session -t NAZWA_SESJI`

`[root@demo]# tmux ls | grep : | cut -d. -f1 | awk '{print substr($1, 0, length($1)-1)}' | xargs kill`



**Przywracanie sesji**

tmux ls - list session (lub tmux list-sessions)

tmux attach - ostatnio zamknięta sesja  (lub tmux a - przywrócenie ostatnio zamkniętej sesji)

tmux a -t NAZWA_SESJI - przywracanie sesji o danej nazwie


**INNE**


tmux list-keys
lists out every bound key and the tmux command it runs

tmux list-commands
lists out every tmux command and its arguments

tmux info
lists out every session, window, pane, its pid, etc.

tmux source-file ~/.tmux.conf
reloads the current tmux configuration (based on a default tmux config)
