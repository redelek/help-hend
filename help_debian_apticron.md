## Jak ustawić auto aktualizacje Debian 9 (Stretch)

Udostępniony: 30 - 06 - 2018 17:19

Debian Linux Guides System Admin

Artykuł ma opisać jak usprawnić pracę administracyjne, za pomocą automatycznych aktualizacji. Do tego celu użyjemy **apticron**.

### Instalacja wymaganych pakietów
``` 
apt -y install unattended-upgrades apt-listchanges apticron
```

### Konfiguracja plików

**edytujemy plik** ```vim /etc/apt/apt.conf.d/50unattended-upgrades```

Poszukaj odpowiednich linii i je odkomentuj lub jeśli ich brakuje poprostu je dopisz. W miejsce **EMAIL** wpisz swój adres e-mail,
na który będą wysyłane powiadomienia.

```
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::Unattended-Upgrade "1";
Unattended-Upgrade::Mail "**EMAIL**";

// Automatically upgrade packages from these 
Unattended-Upgrade::Origins-Pattern {
      "o=Debian,a=stable";
      "o=Debian,a=stable-updates";
      "o=Debian,a=proposed-updates";
      "origin=Debian,codename=${distro_codename},label=Debian-Security";
};

// You can specify your own packages to NOT automatically upgrade here
Unattended-Upgrade::Package-Blacklist {
//      "vim";
//      "libc6";
//      "libc6-dev";
//      "libc6-i686";

};

Unattended-Upgrade::MailOnlyOnError "true";
Unattended-Upgrade::Automatic-Reboot "false";
```

**edytujemy plik** ```vim /etc/apticron/apticron.conf```.

```
EMAIL="**me@example.com**"
DIFF_ONLY="1"
LISTCHANGES_PROFILE="apticron"
SYSTEM="**HOSTNAME.OF.SERVER**"
NOTIFY_HOLDS="0"
NOTIFY_NO_UPDATES="0"
```

**edytujemy plik** ```vim /etc/apt/listchanges.conf```

```
[apt]
frontend=pager
email_address=**me@example**
confirm=0
save_seen=/var/lib/apt/listchanges.db
which=news
```
Testujemy czy działa:

```unattended-upgrade -d```

**Link:**

https://www.vultr.com/docs/how-to-set-up-unattended-upgrades-on-debian-9-stretch
