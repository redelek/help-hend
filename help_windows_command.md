# Drobne porady dla Windows

### Jak sprawdzić jakie są zapamiętane profile WiFi

**_7,8,10_**

Uruchom **cdm** i wpisz :

- lista profili WiFi

```cmd
netsh wlan show profiles
```

- kasowanie zapamiętanego profilu

```cmd
netsh wlan delete profile name="NAZWA PROFILU"
```

lub

```cmd
netsh wlan delete profile name=S*
```
