### Przydatne polecenia dla systemu MacOS X ###

1. Odblokowanie opcji instalacji oprogramowania z dowolnego źródła bez potwierdzeń

```{r, engine='bash', count_lines}
sudo spctl --master-disable
```
```{r, engine='bash', count_lines}
sudo spctl --master-enable
```


