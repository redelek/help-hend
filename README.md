# Potrzebne pliki i informacje


Znajdziemy tutaj przydatne porady dotyczące systemów Linux, MacOS oraz Windows.
Porady nie u wszystkich zadziałają, ale może przybliżą temat i coś lepiej wyjaśnią.

Porady będą rozwijane jeśli chce ktoś się dołączyć zapraszam do kontaktu *** piotr[ET]lx.waw.pl ***


### PLAN PORAD

###### Linux
 - [x] - jak zautomatyzować aktualizacje w linux Debian *[Link to File](help_debian_apticron.md)*
 - [x] - rozpakowywanie i pakowanie katalogów/plików *[Link to File](help_pack_unpack.md)*
 - [ ] - używanie opcji rsync do zadań administracyjnych *...waiting*
 - [x] - używanie wirtualnej konsoli TMUX *[Link to File](help_tmux.md)*
 - [ ] - przydatne polecenia sieciowe (diagnostyka i statystyki) *...waiting*
 - [ ] - postfx zarządzanie kolejkami, przydatne polecenia *...waiting*

###### MacOS

 - [ ] - termianl - jak wyczyścić pamięć DNS cache *...waiting*
 - [ ] - przydatne polecenia *...waiting*

###### Windows

 - [ ] - ... *...waiting*

###### INNE
 
 - [ ] - ...* *
